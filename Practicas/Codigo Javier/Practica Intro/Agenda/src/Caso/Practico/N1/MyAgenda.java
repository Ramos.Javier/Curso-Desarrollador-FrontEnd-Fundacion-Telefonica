package Caso.Practico.N1;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyAgenda 
{

	@SuppressWarnings("resource")
	public static void main(String[] args) 
	{
		List<String> misContactos = new ArrayList<String>();
		String nuevoContacto;
		int posicion;
		
		System.out.println("-------------- My Agenda --------------");	
		boolean seguir = true;
		while(seguir)
		{
			System.out.println("(1) Añadir");
			System.out.println("(2) Eliminar");
			System.out.println("(3) Buscar");
			System.out.println("(4) Salir");
			System.out.println("---------------------------------------");
			System.out.print("Seleccione la operacion: ");
			Scanner entrada = new Scanner(System.in);
			String operacion = entrada.nextLine();
			System.out.println("---------------------------------------");	
			switch(operacion)
			{
				case "1":
					System.out.print("Ingrese nuevo contacto: ");
					nuevoContacto = entrada.nextLine();
					if(misContactos.size()<20) misContactos.add(nuevoContacto);
					else System.out.println("¡Advertencia!: Agenda llena.");
					System.out.println("---------------------------------------");	
				break;
				case "2":
					System.out.println(misContactos);
					System.out.print("Ingrese posicion a eliminar: ");
					posicion = Integer.parseInt(entrada.nextLine());
					if(posicion+1<=misContactos.size())
					{
						misContactos.remove(posicion);
						System.out.println(misContactos);
						System.out.println("---------------------------------------");							
					}
					else
					{
						System.out.println("¡Advertencia!: Posicion NO existe.");
					}
				break;
				case "3":
					System.out.println(misContactos);
					System.out.print("Ingrese posicion a buscar: ");
					posicion = Integer.parseInt(entrada.nextLine());
					if(posicion+1<=misContactos.size())
					{
						System.out.println("Contacto: " + misContactos.get(posicion));						
					}
					else
					{
						System.out.println("¡Advertencia!: Posicion NO existe.");
					}
					System.out.println("---------------------------------------");	
				break;
				case "4":
					seguir = false;
					System.out.println("Fin");
					System.out.println("---------------------------------------");
				break;
				default:
					System.out.println("Error de operacion!!");
					System.out.println("---------------------------------------");
				break;
			}
		}	
	}
	
}