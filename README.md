# Curso-Desarrollador-FrontEnd-Fundacion-Telefonica

Curso de desarrollador FrontEnd en Fundacion Telefonica.

* Modulo 0 - Informacion relevante para el curso
* Modulo 1 - Metodologias de programacion
* Modulo 2 - Diseño e integracion web
* Modulo 3 - Desarrollo de la capa cliente I & II
* Modulo 4 - Desarrollo final de servidor NodeJS
* Practicas - Implementacion de teoria.